package juego;

import java.awt.Color;
//import java.awt.Point;

import entorno.Entorno;

public class Piso {

	Entorno entorno;
	
	// Reprenta el centro del rectagulo
	double centroX;
	double centroY;
	
	double ancho;
	double alto;
	double xDerecha;
	double xIzquierda;
	double ySuperior;
	double yInferior;
	
	public Piso() {}
	
	public Piso(Entorno entorno, double ancho, double alto, double x, double y) {
		this.entorno = entorno;
		this.ancho = ancho;
		this.alto = alto;		
		this.centroX = x;
		this.centroY = y;
		this.ySuperior = centroY - this.alto/2;
		this.yInferior = centroY + this.alto/2;
		this.xIzquierda = centroX - this.ancho/2;
		this.xDerecha = centroX + (ancho/2);
	}
	
	void darCoordenadasX(double xDerecha, double xIzquierda) {
		this.xDerecha = xDerecha;
		this.xIzquierda = xIzquierda;
	};
	
	void darCoordenadasY(double ySuperior, double yInferior) {
		this.ySuperior = ySuperior;
		this.yInferior = yInferior;
	}
	
	void dibujarse() {
		entorno.dibujarRectangulo(centroX, centroY, ancho, alto, 0, Color.gray);
	}
}
