package juego;

// A�adido
import java.awt.Color;
import java.awt.Image;
import java.util.LinkedList;
import java.util.Random;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

// Por defecto
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;

	// A�adir Objetos || ACA VAN A ESTRA LOS OBJETOS USADOS EN EL JUEGO
	Jugador jugador;
	LinkedList<Velociraptor> velociraptos;
	LinkedList<Laser> lasersEnPantalla;
	Piso[] pisos;
	Computadora commodore;

	// Fondos estaticos
	static Image fondo = Herramientas.cargarImagen("multimedia/castillo_800x600.png");
	static Image fondoGameOver = Herramientas.cargarImagen("multimedia/fondoGameOver.jpg");
	static Image fondoWin = Herramientas.cargarImagen("multimedia/fondoWin.jpg");
	static Image icono = Herramientas.cargarImagen("multimedia/icono.png");
	
	// Variables De Juego || PARA CONTROLAR COSAS
	static int anchoPantalla = 800;
	static int altoPantalla = 600;
	static int kills = 0; // Numero de enemigos que mato el jugador
	static Piso pisoActual;

	// Pausa
	static boolean estaEnPausa = false;

	// Gano o Perdio
	static boolean gano = false;
	static boolean finDeJuego = false;
	
	// Sonidos
    static Clip musicaFondo = CargarSonido.cargarSonido("/multimedia/musica_fondo.wav");
    Sonido musica;
    
	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Barbarianna Viking Edition - Grupo 3 - v1", anchoPantalla, altoPantalla);
		this.entorno.setIconImage(icono);
		
		// Inicializar lo que haga falta para el juego
		// ...
		this.jugador = new Jugador(this.entorno);

		// Haciendo Velociraptos
		this.velociraptos = new LinkedList<Velociraptor>();
		for (int i = 1; i <= 6; i++) {
			Velociraptor raptor = new Velociraptor(entorno);
			raptor.x = raptor.x + (i*(raptor.ancho+10));
			this.velociraptos.add(raptor);
		}

		// Haciendo pisos
		this.pisos = new Piso[5];
		double y = 100;
		double x = 500;
		for (int i = 0; i < pisos.length; i++) {
			pisos[i] = new Piso(entorno, 600, 10, x, y);
			y += 128;
			if ((i + 1) % 2 == 0) {
				x = 500;
			} else {
				x = 300;
			}
		}
		
		// Lasers en pantalla
		lasersEnPantalla = new LinkedList<Laser>();

		// Agregando la computadora
		this.commodore = new Computadora(entorno);
		
		// Sonidos
		this.musica = new Sonido(musicaFondo);
		this.musica.cambiarVolumen(50);
		this.musica.loop();
		
		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y por lo
	 * tanto es el método más importante de esta clase. Aquí se debe actualizar
	 * el estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo
		// ...

		// ---- DIBUJAR OBJETOS EN LA PANTALLA ----

		if (!finDeJuego && !estaEnPausa) {
			
			// - FONDO -
			entorno.dibujarImagen(fondo, anchoPantalla / 2, altoPantalla / 2, 0);

			// - JUGADOR -
			// SE DIBUJA DENTRO DE LA CLASE JUGADOR EN MOVIMIENTO

			// - PISOS -
			dibujarPisos();
			
			// - CREAR VELOCIRAPTOR -
			crearRaptor();
			
			// - MOVIMIENTO DE ENEMIGOS --
			moverVelociraptors();

			// - LASERS ENEMIGOS -
			ataquesEnemigos();

			// - COMPUTADORA -
			commodore.dibujarse();

			// ------------ MOVIMIENTO -----------------
			jugador.movimiento();

			// --------- SALTOS | COLISIONES -----------

			// - PISOS Y JUGADOR -
			colisionesJugadorPisos();

			// - ATAQUES DEL JUGADOR -
			ataquesJugador();

			// - JUGADOR Y COMPUTADORA -
			colisionJugadorCommodore();

			// - ENEMIGOS Y JUGADOR -
			colisionJugadorVelociraptors();
			
			// -------------------------------------------

			// - MOSTRAR NUMERO DE ENEMIGOS MATADOS -
			entorno.cambiarFont("Impact", 15, Color.white);
			entorno.escribirTexto("Eliminaciones: " + kills, 10, altoPantalla - 5);

			// - PONER PAUSA AL JUEGO -
			if (entorno.sePresiono(entorno.TECLA_ENTER)) {
				estaEnPausa = true;
			}
			

		} else if (!finDeJuego && estaEnPausa) {
			// - ESTA EN PAUSA -
			pausa();

			// - QUITAR LA PAUSA AL JUEGO -
			if (entorno.sePresiono(entorno.TECLA_ENTER)) {
				estaEnPausa = false;
			}

		} else {
			
			// - FIN DEL JUEGO -
			if (gano) {
				// Win!!
				this.entorno.dibujarImagen(fondoWin, anchoPantalla / 2, altoPantalla / 2, 0);
				this.entorno.cambiarFont("Impact", 30, Color.white);
				if (entorno.sePresiono(entorno.TECLA_ENTER)) {
					System.exit(0);
				}
			} else {
				// Game Over!!
				this.entorno.dibujarImagen(fondoGameOver, anchoPantalla / 2, altoPantalla / 2, 0);
				this.entorno.cambiarFont("Impact", 30, Color.black);
				String texto = "Press Enter to Quit";
				this.entorno.escribirTexto(texto, anchoPantalla / 2 - 110, altoPantalla / 2 + 250);
				if (entorno.sePresiono(entorno.TECLA_ENTER)) {
					System.exit(0);
				}
			}
			
			// ESCRIBIR EN PANTALLA NUMERO DE KILLS
			String texto = "KILLS: " + kills;
			this.entorno.escribirTexto(texto, anchoPantalla / 2 - 50, altoPantalla / 2 + 200);
		}
	}

	// ------------------------------------------------------------

	void dibujarPisos() {
		pisos[0].dibujarse();
		pisos[1].dibujarse();
		pisos[2].dibujarse();
		pisos[3].dibujarse();
		pisos[4].dibujarse();
	}

	void colisionJugadorCommodore() {
		if (commodore.y <= jugador.yInferior && commodore.y >= jugador.ySuperior) {
			if (commodore.x <= jugador.xDerecha && commodore.x >= jugador.xIzquierda) {
				win();
			}
		}
	}

	void colisionesJugadorPisos() {
		for (int i1 = pisos.length - 1; i1 >= 0; i1--) {
			Piso piso = pisos[i1];

			// - REBOTE/CHOQUE DEL JUGADOR CONTRA LOS TECHOS -
			if (jugador.xDerecha > piso.xIzquierda && jugador.xIzquierda < piso.xDerecha) {
				double diferencia = Math.round(jugador.ySuperior) - Math.round(piso.yInferior);
				if (diferencia > 0 && diferencia < 5) {
					jugador.chocoConPiso();
				}
			}

			// - DA (SI ES POSIBLE) UN VALOR A pisoActual DE FORMA QUE EL JUGADOR SE ENCUENRE EN DICHO PISO - 
			if (jugador.yInferior < piso.ySuperior && jugador.ySuperior >= piso.ySuperior - 100) {
				if (jugador.xDerecha >= piso.xIzquierda && jugador.xIzquierda <= piso.xDerecha) {
					pisoActual = piso;
				}
			}
		}

		// - PARA ESTABILIZAR AL JUGADOR EN EL PISO -
		if (pisoActual != null) {
			double diferencia = pisoActual.ySuperior - jugador.yInferior;
			if (diferencia > 0 && diferencia < 10) {
				jugador.yInferior = pisoActual.ySuperior - 10;
				jugador.y = jugador.yInferior - jugador.alto / 2;
				jugador.aterrizar();
			}
		}

		
		// - PARA LOS PISOS QUE ESTAN A LA "IZQUIERDA DE LA PANTALLA" -
		if (pisoActual != null) {
			if (pisoActual.xIzquierda <= 0) {
				if (jugador.xIzquierda > pisoActual.xDerecha) {
					pisoActual = null;
					jugador.caerse();
				}
			}
		}
		// - PARA LOS PISOS QUE ESTAN A LA "DERECHA DE LA PANTALLA" -
		if (pisoActual != null) {
			if (pisoActual.xDerecha >= 800) {
				if (jugador.xDerecha < pisoActual.xIzquierda) {
					pisoActual = null;
					jugador.caerse();
				}
			}
		}
	}

	void ataquesJugador() {
		// - RECORRE LOS RALAMPAGOS DEL JUGADOR: LOS QUITA SI MATA UN RAPTOR O SE VAN DE RANGO -
		for (int i1 = 0; i1 < jugador.relampagos.size(); i1++) {
			Relampago relampago = jugador.relampagos.get(i1);
			if (relampago.fueraDeRango || relampagoMatoRaptor(i1)) {
				jugador.relampagos.remove(i1);
			}
		}
	}

	boolean relampagoMatoRaptor(int relamapago_index) {
		// - RETORNA TRUE SI EL RELAMAPAGO MATO AL RAPTOR, SUMA UNA KILL Y ELIMINA AL RAPTOR -
		Relampago relampago = jugador.relampagos.get(relamapago_index);
		for (int i = 0; i < velociraptos.size(); i++) {
			if (velociraptos != null) {
				Velociraptor velociraptor = velociraptos.get(i);
				if ((velociraptor.y - velociraptor.alto / 2) <= relampago.posY
						&& relampago.posY <= velociraptor.parteInferior) {
					if ((relampago.posX + relampago.ancho / 2) <= velociraptor.ladoDerecho
							&& (relampago.posX - relampago.ancho / 2) >= velociraptor.ladoIzquierdo) {
						eliminarRaptor(i);
						kills++;
						return true;
					}
				}
			}
		}
		return false;
	}
	void eliminarRaptor(int raptor_index) {
		velociraptos.remove(raptor_index);
	}

	// - PAUSA EL JUEGO -
	void pausa() {
		entorno.dibujarImagen(fondo, anchoPantalla / 2, altoPantalla / 2, 0);
		entorno.cambiarFont("Impact", 30, Color.LIGHT_GRAY);
		entorno.escribirTexto("JUEGO EN PAUSA: PRESIONA ENTER PARA VOLVER", anchoPantalla / 2 - 260, altoPantalla / 2);
		if (entorno.sePresiono(entorno.TECLA_ESPACIO)) {
			estaEnPausa = false;
		}
	}

	// - FINALIZA EL JUEGO Y ACLARA QUE EL JUGADOR GANO -
	void win() {
		GameOver();
		gano = true;
	}

	// GAME OVER: EL JUGADOR HA PERDIDO
	void GameOver() {
		finDeJuego = true;
		while(!velociraptos.isEmpty()) {
			velociraptos.removeFirst();
		}
	}

	
	// - DA UN PISO A CADA RAPTOR, ESTE SERA EL PISO EN EL QUE SE ENCUENTRE DICHO RAPTOR -
	void darPisoAraptor(Velociraptor raptor) {

		if (raptor.y > 530) {
			raptor.pisoActual = new Piso(entorno, 800, 10, anchoPantalla / 2 + 30, altoPantalla / 2);
			return;
		}

		for (Piso piso : pisos) {
			double diferencia = piso.ySuperior - raptor.parteInferior;
			if (diferencia >= 0 && diferencia <= 3) {
				raptor.pisoActual = piso;
				raptor.y = piso.ySuperior - raptor.alto / 2;
				raptor.cayendo = false;
				break;
			}
		}
	}
	// - INDICA QUE EL RAPTOR DEBERA CAERSE AL SUPERR CIERTOS LIMITES DE SU PISO -
	boolean raptorDebeCaerse(Velociraptor raptor) {
		if (raptor.pisoActual.xIzquierda == 0) {
			if (raptor.pisoActual.xDerecha < raptor.ladoIzquierdo) {
				return true;
			}
		} else {
			if (raptor.pisoActual.xIzquierda > raptor.ladoDerecho
					&& raptor.pisoActual.xDerecha > raptor.ladoIzquierdo) {
				return true;
			}
		}
		return false;
	}
	// - SE ENCARGA DEL MOVIMIENTO DE LOS VELOCIRAPTORS, APOYANDOSE DE METODOS AUXILIARES -
	void moverVelociraptors() {
		for (int i = 0; i < velociraptos.size(); i++) {
			Velociraptor raptor = velociraptos.get(i);
			darPisoAraptor(raptor);
			if (raptor.pisoActual != null) {
				if (raptorDebeCaerse(raptor)) {
					raptor.caer();
					raptor.movimiento();
				} else {
					raptor.movimiento();
				}
			}
			
			if (raptor.fueraDeRango) {
				velociraptos.remove(i);
			}
		}
	}
	// - SE ENCARGA DE LA COLISION ENTRE JUGADOR Y VELOCIRAPTORS -
	void colisionJugadorVelociraptors() {
		for (Velociraptor raptor : velociraptos) {
			if (jugador.ySuperior < raptor.parteInferior) {
				if ((jugador.xDerecha < raptor.ladoDerecho && jugador.xDerecha > raptor.ladoIzquierdo)
						|| (jugador.xIzquierda < raptor.ladoDerecho && jugador.xIzquierda > raptor.ladoIzquierdo)) {
					GameOver();
					break;
				}
			}
		}
	}
	
	// - SE ENCARGA DE MOVER, CREAR Y QUITAR LOS LASERS DE LOS ENEMIGOS EN PANTALLA -
	void ataquesEnemigos() {
		Random tiroRandom = new Random();
		for (Velociraptor raptor : velociraptos) {
			if(tiroRandom.nextInt(100) <= 20 && raptor.enfriamento >= 200) {
				raptor.enfriamento = 0;
				lasersEnPantalla.addLast(new Laser(entorno, raptor));
			}		
		}
		
		for(int i = 0; i < lasersEnPantalla.size(); i++) {
			Laser laser = lasersEnPantalla.get(i);
			laser.movimiento();
			
			if(laser.x <= jugador.xDerecha && laser.x >= jugador.xIzquierda &&
					laser.y <= jugador.yInferior && laser.y >= jugador.ySuperior) {
				GameOver();
				return;
			}
			
			if(laser.fueraDeRango) {
				lasersEnPantalla.remove(i);
			}
		}
	}
	
	// - CREA UN VELOCIRAPTOR SI ES NECESARIO (SI LOS VELOCIRAPTORS EN PANTALLA SON MENORES QUE 4) -
	void crearRaptor() {
		double posicionJugadorY = 800;
		if(pisoActual != null) {
			posicionJugadorY = pisoActual.ySuperior;
			}
		// Para que siempre haya 4 velociraptors en pantalla
		if(velociraptos.size() <= 4 && posicionJugadorY >= 300) {
			Velociraptor raptor = new Velociraptor(entorno);
			// Separa a los raptors
			raptor.x = raptor.x + (2*(raptor.ancho+10));
			this.velociraptos.add(raptor);
			return;
		}
		// Esto lo hago para que el jugador pueda ganar.
		if(posicionJugadorY < 300 && velociraptos.size() <= 2) {
			Random random = new Random();
			Velociraptor raptor = new Velociraptor(entorno);
			raptor.x = raptor.x + (2*(raptor.ancho+10));
			this.velociraptos.add(raptor);
			return;
		}
	}

	/* <------------------ FIN ------------------------> */

	@SuppressWarnings("unused")
	public static void main(String[] args){

		// --- MANEJO DE ERRORES CON TRY/CATCH ---
		try {
			Juego juego = new Juego();
		} catch (Exception error) {
			System.out.println(error.getMessage());
		}
		
	}

	
}
