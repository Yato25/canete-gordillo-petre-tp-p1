package juego;

import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class Sonido {
	
	// Variables donde guardar el sonido, y una variable de control de volumen
	private Clip clip;
	private FloatControl volumen;
	
	// Constructor
	public Sonido() {}
	
	public Sonido(Clip clip){
		this.clip = clip;
		this.volumen = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
	}
	
	public void loop() {
		clip.setFramePosition(0);
		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}
	
	public void play() {
		this.clip.setFramePosition(0);
		clip.start();
	}
	
	public void stop() {
		clip.stop();
	}
	
	public int getFrametPosition() {
		return clip.getFramePosition();
	}

	public void cambiarVolumen(float v) {
		// El valor limite va desde    min -80 max 6.0206
		float conversionVol = v - 94;
		if(conversionVol < -80) {
			conversionVol = -80;
		}
		if(conversionVol > 6) {
			conversionVol = 6;
		}
		volumen.setValue(conversionVol);
	}
}
