package juego;

//import java.awt.Color;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.LinkedList;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;

public class Velociraptor {
	
	//Variables de instancia
	Rectangle rectangulo;
	Entorno entorno;
	double x; double y; double ancho; double alto;
	double velDeMovimiento;
	double velDeCaida;
	Point ultimoPuntoDeDerecha;
	Point ultimoPuntoDeIzquierda;
	
	double ladoDerecho;
	double ladoIzquierdo;
	double parteInferior;
	double parteSuperior;
	
	Piso pisoActual;
	Piso pisoSiguiente;
	
	boolean cayendo;
	boolean derecha;
	
	boolean fueraDeRango;
	
	// Laser
	LinkedList<Laser> lasers;
	int enfriamento;
	
	// Random Velocidad
	static Random velRandom = new Random();
	
	// Multimedia
	Image imagen_derecha;
	Image imagen_izquierda;
	
	//Constructores
	public Velociraptor() {}
	
	public Velociraptor(Entorno entorno) {
		this.entorno = entorno;
		this.x = 750.0; this.y = 55;
		this.ancho = 140.0; this. alto = 80;	
		this.velDeMovimiento = velRandom.nextInt(3) + 1;
		this.velDeCaida = 5;
		this.ladoDerecho = x + ancho/2;
		this.ladoIzquierdo = x - ancho/2;
		this.parteInferior = y + alto/2;
		this.parteSuperior = y - alto/2;
		this.imagen_derecha = Herramientas.cargarImagen("multimedia/velociraptor_derecha.png");
		this.imagen_izquierda = Herramientas.cargarImagen("multimedia/velociraptor_izquierda.png");
		this.cayendo = false;
		this.derecha = false;
		this.lasers = new LinkedList<Laser>();
		this.enfriamento = 100;
	}
	
	// --------- AUTODIBUJARSE ------------
//	Prototipo
//	void dibujarse() {
//		entorno.dibujarRectangulo(x, y, ancho, alto, 0, Color.BLUE);
//	}

//  Dibujar	
	void dibujarseConImagen() {
		if(!derecha) {
			entorno.dibujarImagen(imagen_izquierda, x, y, 0, 0.12);
		}else {
			entorno.dibujarImagen(imagen_derecha, x, y, 0, 0.12);
		}
	}
	// - - - - - - - - - - - - - - - - - - -
		
	// ----------- MOVIMIENTO -----------------
	void movimiento() {
		// Dibujar el Velociraptor
		dibujarseConImagen();
		
		// Actualiza los lados 
		actualizarPosiciones();
		
		// Controla el movimiento para ir alternando entre piso.
		if(pisoActual.xIzquierda == 0) {
			moverseDerecha();
			this.derecha = true;
		}else if(pisoActual.xDerecha >= 800) {
			moverseIzquierda();
			this.derecha = false;
		}else{
			this.x -= velDeMovimiento;
		}
		
		
		// Si se va de rango en la planta baja
		if(pisoActual.ancho == 800) {
			if(this.x < - this.ancho) {
				fueraDeRango = true;
			}
		}
		
		// Controla el tiempo en el que salen los ataques
		if(enfriamento <= 200) {
			enfriamento++;
		}
	}
	
	void moverseIzquierda() {
		this.x -= velDeMovimiento;
	}
	
	void moverseDerecha() {
		this.x += velDeMovimiento;
	}
	
	void caer() {
		this.y += velDeCaida;
		this.cayendo = true;
	}
	
	void actualizarPosiciones() {
		ladoDerecho = this.x + ancho/2;
		ladoIzquierdo = this.x - ancho/2;
		parteInferior = this.y + alto/2;
	}
}
