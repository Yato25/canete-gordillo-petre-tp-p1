package juego;

import java.awt.Color;
import java.awt.Rectangle;
import entorno.Entorno;

public class Laser {
	Entorno entorno;
	Rectangle laser;
	double x;
	double y;
	double ancho;
	double alto;
	double velDeMovimiento;
	boolean fueraDeRango;
	boolean derecha;

	public Laser() {
	}

	public Laser(Entorno entorno, Velociraptor raptor) {
		this.entorno = entorno;
		this.laser = new Rectangle();
		this.x = raptor.x;
		this.y = raptor.y - 22;
		this.ancho = 20;
		this.alto = 5;
		this.fueraDeRango = false;
		if (raptor.derecha) {
			derecha = true;
			this.velDeMovimiento = 15;
		} else {
			derecha = false;
			this.velDeMovimiento = -15;
		}
	}

	public void dibujarse() {
		entorno.dibujarRectangulo(x, y, ancho, alto, 0, Color.RED);
	}

	public void movimiento() {

		dibujarse();

		x += velDeMovimiento;

		if (x > 800 || x < 0) {
			fueraDeRango = true;
		}
	}

}
