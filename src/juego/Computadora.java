package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Computadora {

	
	Entorno entorno;
	Image imagen;
	double x;
	double y;
	
	Computadora(){}
	
	Computadora(Entorno entorno){
		this.entorno = entorno;
		this.imagen = Herramientas.cargarImagen("multimedia/computadora_.png");
		this.x = (entorno.ancho()/2) + 200; 
		this.y = 50;
	}
	
	void dibujarse() {
		entorno.dibujarImagen(imagen, x, y, 0);
	}
	
}
