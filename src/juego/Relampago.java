package juego;

import java.awt.Color;
import java.awt.Rectangle;

import entorno.Entorno;

public class Relampago {

	// Pizarra
	Entorno entorno;
	Rectangle imagenRectangulo;
	
	// Propiedades 
	double alto;
	double ancho;
	double posX;
	double posY;
	boolean derecha;
	boolean fueraDeRango;
	
	// Movimiento
	double dx;
	
	Relampago(){}

	Relampago(Entorno entorno, Jugador jugador){
		this.entorno = entorno;
		this.alto = 3;
		this.ancho = 20;
		this.dx = 10;
		this.posX = jugador.derecha? jugador.xDerecha : jugador.xIzquierda;
		this.posY = jugador.y;
		this.derecha = jugador.derecha;
		this.fueraDeRango = false;
	}
	
	void dibujarse() {
		entorno.dibujarRectangulo(posX, posY, ancho, alto, 0, Color.yellow);
	}
	
	void moverse() {
		if(derecha) {
			posX += dx;
		}else {
			posX -= dx;
		}
		if(posX > entorno.ancho() || posX < 0) {
			this.fueraDeRango = true;
		}
	}
	
	boolean equals(Relampago otro) {
		return this.posX == this.posX && this.posY == otro.posY;
	}
	
	
}
