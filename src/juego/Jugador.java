package juego;

// Los sprites funcionan. falta sprite: salto, disparo, agacharse, muere.
// Problema: al actualizarse la pantalla hay titileos en los gif(sprites)

import java.awt.Color; // no se usa pero podria servir
import java.awt.Image;
import java.awt.Rectangle; // cambie el rectangulo por la imagen
import java.util.LinkedList;

import entorno.Entorno;
import entorno.Herramientas;

public class Jugador {

	// --- VARIABLES DE INSTANCIA ---
	Rectangle imagen; // Agregue los sprites pero nos los usare, voy a realizar la logica
	Entorno entorno;

	// Centro
	double x;
	double y;

	// Posiciones
	double ySuperior;
	double yInferior;
	double xIzquierda;
	double xDerecha;

	// Velocidad
	double velocidad_x;
	double velocidad_y;
	double dy;

	// Salto
	double salto;
	boolean saltando;
	int saltos;

	// Caida
	boolean estaCayendo;
	double caerEn;

	// Propiedades
	double ancho;
	double alto;
	double altoNormal;
	double altoAgachado;

	// Ataque
	LinkedList<Relampago> relampagos = new LinkedList<Relampago>();
	int ataquesDisponibles;
	int enfriamiento;

	// - Multimedia -
	Image sprite_d;
	Image sprite_i;
	Image sprite2;
	Image sprite3;
	Image spriteA_d;
	Image spriteA_i;
	Image imagenAgachada;
	Image imagenActual;
	static double x_aux = 0;
	static double y_aux = 0;
	boolean derecha;
	static boolean moviendo;
	static boolean parado;
	static boolean agachado;

	// Constructores
	public Jugador() {
	}

	public Jugador(Entorno entorno) {
		// -- VARIABLES DE INSTANCIA --
		this.entorno = entorno; // Entorno sera nuestra pizarra

		// posiciones en x e y
		this.x = 50;
		this.y = 545;

		// variables propiedades de jugador
		this.ancho = 50; // Esto debera ser considerado en las colisiones..
		this.alto = 60.0; // ..esto tambien
		this.ySuperior = y - alto / 2;
		this.yInferior = y + alto / 2;
		this.xIzquierda = x - ancho / 2;
		this.xDerecha = x + ancho / 2;
		this.salto = 10.0;
//		this.velocidadMovimiento = 5;      // Lo cambiaron por vel_x e vel_y
		this.velocidad_x = 5.0;
		this.velocidad_y = 0.0;
		this.dy = 0.35;
		this.saltando = false;
		this.saltos = 1;
		this.estaCayendo = false;
		
		this.altoNormal = 60.0;
		this.altoAgachado = 60.0 / 2;

		// carga de los diferentes sprites ( falta mejorar )
		this.sprite_d = Herramientas.cargarImagen("multimedia/parado_derecho.gif");
		this.sprite_i = Herramientas.cargarImagen("multimedia/parado_izquierdagif.gif");
		this.sprite2 = Herramientas.cargarImagen("multimedia/movimiento_derecha.gif");
		this.sprite3 = Herramientas.cargarImagen("multimedia/movimiento_izquierdo.gif");
//		this.spriteA_d = Herramientas.cargarImagen("multimedia/agachada_derecha.gif");
//		this.spriteA_i = Herramientas.cargarImagen("multimedia/agachada_izquierda.gif");
		this.imagenActual = sprite_d;
//		this.imagenAgachada = spriteA_d;
		x_aux = this.x;
		y_aux = this.y;
		derecha = true;
		agachado = false;
		parado = true;
		
		// Ataque
		this.ataquesDisponibles = 6;
		this.enfriamiento = 300;
		this.caerEn = 540;
	}

	// --------- AUTODIBUJARSE ------------
	void dibujarse() {
		if (parado) {
//			entorno.dibujarRectangulo(this.x, this.y, ancho, alto, 0, Color.gray);
		} else {
			entorno.dibujarRectangulo(this.x, this.y + 20, ancho, alto, 0, Color.getHSBColor( 26, 67, 49 ));
		}

	}

	void dibujarseConSprite() {
		if (parado) {
			entorno.dibujarImagen(imagenActual, this.x, this.y, 0); // Voy a enfocarme en la logica por ahora
		} else {
//			entorno.dibujarImagen(imagenAgachada, this.x, this.y, 0);
		}

	}

	// - - - - - - - - - - - - - - - - - - -

	// ---------------- MOVIMIENTO -----------------------
	void movimiento() {

		actualizarPosiciones();
		actualizarAtaques();
		dibujarse();
		dibujarseConSprite();

		/*
		 * Mientras se mantenga presionada la tecla, se movera.
		 */

		// - CONTROL DE LA IMAGEN / CAMBIOS DE SPRITE -
		if ((entorno.estaPresionada('d') || entorno.estaPresionada('a')) && !entorno.estaPresionada('s')) {
			if (entorno.estaPresionada('d')) {
				// Si no se choca contra limite derecho
				if (this.x <= 795 - this.ancho / 2) {
					moverseDerecha();
					imagenActual = sprite2; // Cambia la imagen mirando a la derecha
					derecha = true;
				}
			}
			if (entorno.estaPresionada('a')) {
				// Si no se choca contra limite izquierdo
				if (this.x > 0 + this.ancho / 2) {
					moverseIzquierda();
					imagenActual = sprite3; // Cambia la imagen mirando a la izquierda
					derecha = false;
					moviendo = true;
				}
			}
		} else {
			if (derecha) {
				imagenActual = sprite_d;
			} else {
				imagenActual = sprite_i;
			}
		}

		if (entorno.estaPresionada('s')) {
			parado = false;
			alto = altoAgachado;
		} else {
			parado = true;
			alto = altoNormal;
		}

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		// - COLISIONES CON OTROS OBJETOS (AUN NO CREADOS) -

		// OBJETOS contra los que se choca: pisos, velociraptors, computadoraCommodere.
		// Mueve al personaje dependiendo de la velocidad en y

		// Este if controla: que se presione 'u' y que solo salte una vez

		if ((entorno.sePresiono('w') || entorno.sePresiono('u')) && saltos == 1 && parado && !estaCayendo) {
			saltando = true;
			saltos = 0;
			velocidad_y -= salto;
		}

		// Controla los altos y la caida 
		if (saltando || estaCayendo) {
			velocidad_y += dy;
			y += velocidad_y;
		}

		// Este if reaccionaria para evitar irse al vacio.y tambien como aterrizaje en cada piso
		if (y > caerEn) {
			saltos = 1;
			saltando = false;
			estaCayendo = false;
			y = caerEn;
			velocidad_y = 0;
		}

		// --- ATAQUE ---
		if (entorno.sePresiono('E') && ataquesDisponibles > 0 && parado) {
			Relampago nuevoAtaque = new Relampago(entorno, this);
			relampagos.add(nuevoAtaque);
			ataquesDisponibles--;
			enfriamiento -= 50;
		}
	}

	private void moverseDerecha() {
		x += velocidad_x;
	}

	private void moverseIzquierda() {
		x -= velocidad_x;
	}

	void chocoConPiso() {
		velocidad_y = 2;
	}

	void aterrizar() {
		dy = 0.35;
		saltando = false;
		estaCayendo = false;
		velocidad_y = 0;
		saltos = 1;
	}

	void caerse() {
		velocidad_y = 1;
		this.estaCayendo = true;
		this.saltos = 0;
	}

	void actualizarPosiciones() {
		this.ySuperior = y - alto / 2;
		this.yInferior = y + alto / 2;
		this.xIzquierda = x - ancho / 2;
		this.xDerecha = x + ancho / 2;
	}

	void actualizarAtaques() {
		ataquesDisponibles = enfriamiento/50;
		if (enfriamiento >= 0 && enfriamiento < 200) {
			enfriamiento++;
		}
		
		if (!relampagos.isEmpty()) {
			for (Relampago relampago : relampagos) {
				relampago.dibujarse();
				relampago.moverse();
			}
		}
	}

	// --------------------------------------------------------------

}
